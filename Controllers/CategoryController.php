<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //echo "Add new Record";
        return view('category_add')->with('catArr', Category::All());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo $request->input('parent_category');exit;
        $res = new Category;
        $res->category_name = $request->input('category_name');
        $res->category_slug = $request->input('category_slug');
        $res->parent_id = $request->input('parent_category');        
        $res->save();
        $request->session()->flash('msg', "New record added successfully");
        return redirect('category/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('categories_show')->with('catArr', Category::All());
    }    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category, $id)
    {
        //echo $id;exit;
        $all_category = Category::where('id', '!=' , $id)->orWhereNull('id')->get();
        //print_r($all_category);exit;
        //return view('category_edit')->with('categoryArr', Category::find($id));
        return view('category_edit')->with(['categoryArr' => Category::find($id),  'Categories' => $all_category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */    
    public function update(Request $request, Category $category)
    {
        //echo $request->input('name');
        $res = Category::find($request->id);
        $res->category_name=$request->input('category_name');
        $res->category_slug=$request->input('category_slug');
        $res->parent_id = $request->input('parent_category');
        $res->save();

        $request->session()->flash('msg', 'Record Updated');
        return redirect('category/list');       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //Do the soft delete
    }
}
