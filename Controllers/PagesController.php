<?php

namespace App\Http\Controllers;

use App\Models\Pages;
use App\Models\Category;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo 'This is Manage Pages';exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page_add')->with('catArr', Category::All());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo $request->input('parent_category');exit;
        $res = new Pages;
        $res->page_name = $request->input('page_name');
        $res->page_title = $request->input('page_title');
        $res->page_slug = $request->input('page_slug');
        $res->parent_id = $request->input('parent_id');
        $res->content = $request->input('content');        
        $res->save();
        $request->session()->flash('msg', "New page added successfully");
        return redirect('page/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function show(Pages $pages)
    {
        //echo 'This is Manage Pages';exit;
        //return view('pages_list')->with('pageArr', Pages::All());
        $pages = Pages::join('categories', 'pages.parent_id', '=', 'categories.id')
               ->get(['pages.*', 'categories.category_name']);
        return view('pages_list')->with('pageArr', $pages);
                       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function edit(Pages $pages, $id)
    {
        return view('page_edit')->with('pageArr', Pages::find($id))->with('catArr', Category::All());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pages $pages)
    {
        //echo $request->input('name');
        $res = Pages::find($request->id);
        $res->page_name = $request->input('page_name');
        $res->page_title = $request->input('page_title');
        $res->page_slug = $request->input('page_slug');
        $res->parent_id = $request->input('parent_id');
        $res->content = $request->input('content');
        $res->save();
        $request->session()->flash('msg', 'Page Updated');
        return redirect('page/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pages $pages)
    {
        //
    }
    public static function page_menu()
    {
        $result = Category::where(['parent_id' => 0], ['is_active' => 1])->get();
        return $result;       

    }
    public static function pages($category_id='')
    {
        //echo $category_id;exit;
        $res_cat = Category::where(['id' => $category_id], ['is_active' => 1])->get();        
        $data['category_name'] = $res_cat[0]['category_name'];

        $res_pages = Pages::where(['parent_id' => $category_id], ['is_active' => 1])->get();        
        $data['page_content'] = $res_pages[0]['content']??'';

        $data['result'] = Category::where(['parent_id' => $category_id], ['is_active' => 1])->get();
        //print_r($data);exit;
        return view('front.pages', $data);
    }
}
