<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::view('/', 'front.home');
Route::get('/pages/{id}', 'App\Http\Controllers\PagesController@pages');

//Create routes for Crud
//http://127.0.0.1:8000/crud_edit/1
Route::get('crud_show','App\Http\Controllers\CrudController@show');
Route::get('crud_delete/{id}', 'App\Http\Controllers\CrudController@destroy');
Route::get('crud_add', 'App\Http\Controllers\CrudController@create');
Route::post('crud_store', 'App\Http\Controllers\CrudController@store');

Route::get('crud_edit/{id}', 'App\Http\Controllers\CrudController@edit');
Route::post('crud_update/{id}', 'App\Http\Controllers\CrudController@update')->name('crud.update');
Route::view("noaccess", "noaccess");
Route::view("test", "test");
Route::get("event", "App\Http\Controllers\UsersAuth@index");

//Create routes for CMS
//https://gitlab.com/lokeshdas248/test_laravel_cms
//http://127.0.0.1:8000/category/list
Route::get('category/list','App\Http\Controllers\CategoryController@show');
Route::get('category/add', 'App\Http\Controllers\CategoryController@create');
Route::post('category/store', 'App\Http\Controllers\CategoryController@store');
Route::get('category/edit/{id}', 'App\Http\Controllers\CategoryController@edit');
Route::post('category_update/{id}', 'App\Http\Controllers\CategoryController@update')->name('category.update');
//http://127.0.0.1:8000/page/add
Route::get('page/list','App\Http\Controllers\PagesController@show');
Route::get('page/add', 'App\Http\Controllers\PagesController@create');
Route::post('page/store', 'App\Http\Controllers\PagesController@store');
Route::get('page/edit/{id}', 'App\Http\Controllers\PagesController@edit');
Route::post('page_update/{id}', 'App\Http\Controllers\PagesController@update')->name('page.update');

