<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Page List</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

        <!-- Styles -->
        <style>
        
        </style>

        
    </head>
    <body>
        <p style="text-align:right;padding:20px;"><a href="add">Add New Page</a>
        </p>
        <p style="text-align:center;" class="text-success">{{session('msg')}}</p>
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">PAGE</th>
              <th scope="col">CATEGORY</th>
              <th scope="col">TITLE</th>
              <th scope="col">SLUG</th>              
              <th scope="col">ACTION</th>
            </tr>
          </thead>
          <tbody>
          @foreach($pageArr as $page)       
            <tr>
                <td>{{$page->id}}</td>
                <td>{{$page->page_name}}</td>
                <td>{{$page->category_name}}</td>
                <td>{{$page->page_slug}}</td>
                <td>{{$page->page_title}}</td>                
                <td><a href="edit/{{$page->id}}">Edit</a> / 
                  <a href="delete/{{$page->id}}">Delete</a></td>
            </tr>
            @endforeach 
            

        </table>
    </body>
</html>
