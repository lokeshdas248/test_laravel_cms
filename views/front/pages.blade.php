@extends('front/layout.layout')

@section('page_title', $category_name)
@section('page_name', $category_name)


@section('container')
<h4>SUB CATEGORIES</h4>
<ul class="list-inline">
@foreach($result as $category)
	<li><a class="btn btn-color" href="{{url('/pages/'.$category->id)}}">{{$category->category_name}}</a></li>
@endforeach
</ul>
<h4>DETAILS</h4>
{{$page_content}}
@endsection
