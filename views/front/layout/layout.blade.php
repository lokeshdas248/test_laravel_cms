<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>@yield('page_title') | CMS</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Fav icon -->
	<link rel="shortcut icon" href="img/favicon.ico">


	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,900,700,700italic,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700,600' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,300,500,700' rel='stylesheet' type='text/css'>
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

	<link rel="stylesheet" href="{{ asset ('assets/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset ('assets/css/skin-lblue.css') }}">
	<link rel="stylesheet" href="{{ asset ('assets/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset ('assets/css/normalize.css') }}">

	<link href="{{ asset ('assets/plugins/et-line/et-line.css') }}" rel="stylesheet" type="text/css"/>
	<!-- Themify icons -->
	<link href="{{ asset ('assets/css/themify-icons/themify-icons.css') }}" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="{{ asset ('assets/css/owl.carousel.css') }}">
	<link rel="stylesheet" href="{{ asset ('assets/css/main.css') }}">
	<link rel="stylesheet" href="{{ asset ('assets/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset ('assets/css/setting.css') }}">
	<link rel="stylesheet" href="{{ asset ('assets/css/responsive.css') }}">
	<script src="{{ asset ('assets/js/vendor/modernizr-2.6.2.min.js') }}"></script>

	<script src="{{ asset ('assets/js/vendor/jquery-1.10.2.min.js')}}"></script>
	<script src="{{ asset ('assets/js/bootstrap.min.js')}}"></script>
	<script src="{{ asset ('assets/js/smoothscroll.js')}}"></script>
	<!-- Scroll up js
	============================================ -->
	<script src="{{ asset ('assets/js/jquery.scrollUp.js')}}"></script>
	<script src="{{ asset ('assets/js/menu.js')}}"></script>
	<script src="{{ asset ('assets/js/owl.carousel.min.js')}}"></script>
	<script src="{{ asset ('assets/js/jquery.collapse.js')}}"></script>

	<script type="text/javascript" src="{{ asset ('assets/plugins/counter/waypoints.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset ('assets/plugins/counter/jquery.counterup.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset ('assets/js/counters.js')}}"></script>

	<script src="{{ asset ('assets/js/main.js')}}"></script>
</head>

<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Start Loading -->
<section class="loading-overlay">
	<div class="Loading-Page">
		<h1 class="loader">Loading...</h1>
	</div>
</section>
<!-- End Loading  -->

<!-- start header -->
<header>
	<!-- Top bar starts -->
	<div class="top-bar">
		<div class="container">

			<!-- Contact starts -->
			<div class="tb-contact pull-left">
				<!-- Email -->
				<i class="fa fa-envelope color"></i> &nbsp; <a href="#">contact@testingwebsite.com</a>
				&nbsp;&nbsp;
				<!-- Phone -->
				<i class="fa fa-phone color"></i> &nbsp; +1 (342)-(323)-4923
			</div>
			<!-- Contact ends -->

			<!-- Shopping kart starts -->
			<div class="tb-shopping-cart pull-right">
				<!-- Link with badge -->
				<a href="#" class="btn btn-white btn-xs b-dropdown"><i class="fa fa-shopping-cart"></i> <i class="fa fa-angle-down color"></i> <span class="badge badge-color">2</span></a>
				<!-- Dropdown content with item details -->
				<div class="b-dropdown-block">
					<!-- Heading -->
					<h4><i class="fa fa-shopping-cart color"></i> Your Items</h4>
					<ul class="list-unstyled">
						<!-- Item 1 -->
						<li>
							<!-- Item image -->
							<div class="cart-img">
								<a href="#"><img src="img/ecommerce/view-cart/1.png" alt="" class="img-responsive" /></a>
							</div>
							<!-- Item heading and price -->
							<div class="cart-title">
								<h5><a href="#">Premium Quality Shirt</a></h5>
								<!-- Item price -->
								<span class="label label-color label-sm">$1,90</span>
							</div>
							<div class="clearfix"></div>
						</li>
						<!-- Item 2 -->
						<li>
							<div class="cart-img">
								<a href="#"><img src="img/ecommerce/view-cart/2.png" alt="" class="img-responsive" /></a>
							</div>
							<div class="cart-title">
								<h5><a href="#">Premium Quality Shirt</a></h5>
								<span class="label label-color label-sm">$1,20</span>
							</div>
							<div class="clearfix"></div>
						</li>
					</ul>
					<a href="#" class="btn btn-white btn-sm">View Cart</a> &nbsp; <a href="#" class="btn btn-color btn-sm">Checkout</a>
				</div>
			</div>
			<!-- Shopping kart ends -->

			<!-- Langauge starts -->
			<div class="tb-language dropdown pull-right">
				<a href="#" data-target="#" data-toggle="dropdown"><i class="fa fa-globe"></i> English <i class="fa fa-angle-down color"></i></a>
				<!-- Dropdown menu with languages -->
				<ul class="dropdown-menu dropdown-mini" role="menu">
					<li><a href="#">Germany</a></li>
					<li><a href="#">France</a></li>
					<li><a href="#">Brazil</a></li>
				</ul>
			</div>
			<!-- Language ends -->

			<!-- Search section for responsive design -->
			<div class="tb-search pull-left">
				<a href="#" class="b-dropdown"><i class="fa fa-search square-2 rounded-1 bg-color white"></i></a>
				<div class="b-dropdown-block">
					<form>
						<!-- Input Group -->
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Type Something">
									<span class="input-group-btn">
										<button class="btn btn-color" type="button">Search</button>
									</span>
						</div>
					</form>
				</div>
			</div>
			<!-- Search section ends -->

			<!-- Social media starts -->
			<div class="tb-social pull-right">
				<div class="brand-bg text-right">
					<!-- Brand Icons -->
					<a href="#" class="facebook"><i class="fa fa-facebook square-2 rounded-1"></i></a>
					<a href="#" class="twitter"><i class="fa fa-twitter square-2 rounded-1"></i></a>
					<a href="#" class="google-plus"><i class="fa fa-google-plus square-2 rounded-1"></i></a>
				</div>
			</div>
			<!-- Social media ends -->

			<div class="clearfix"></div>
		</div>
	</div>

	<!-- Top bar ends -->

	<!-- Header One Starts -->
	<div class="header-1">

		<!-- Container -->
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<!-- Logo section -->
					<div class="logo">
						<h1><a href="index.html"><i class="fa fa-bookmark-o"></i> Company Logo</a></h1>
					</div>
				</div>
				<div class="col-md-6 col-md-offset-2 col-sm-5 col-sm-offset-3 hidden-xs">
					<!-- Search Form -->
					<div class="header-search">
						<form>
							<!-- Input Group -->
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Type Something">
										<span class="input-group-btn">
											<button class="btn btn-color" type="button">Search</button>
										</span>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- Navigation starts -->

		<div class="navi">
			<div class="container">
				<div class="navy">
					<ul>
						<li><a href="{{url('/')}}">Home</a>
						<?php
						use  App\Http\Controllers\PagesController;
						$result = PagesController::page_menu();
						//echo '<pre>';
						//print_r($result);
						?>
						<!-- Main menu -->
						@foreach($result as $menu)
						<li><a href="{{url('/pages/'.$menu->id)}}">{{$menu->category_name}}</a>
						@endforeach
						
					</ul>
				</div>
			</div>
		</div>

		<!-- Navigation ends -->

	</div>

	<!-- Header one ends -->

</header>
<!-- end header -->

<!-- start main content -->
<main class="main-container">
	<!-- service section -->
	<div class="container">
		<!-- What We Do -->
		<div class="content-md container-sm border-bottom-1">
			<div class="row">
				<div class="col-sm-7 sm-margin-b-50">
					<!-- Heading v1 -->
					<div class="heading-v1 margin-b-30">
						<h2 class="heading-v1-title">@yield('page_name')</h2>
						<p class="heading-v1-subtitle"></p>
					</div>
					<!-- End Heading v1 -->

					<div class="margin-b-30">
						<p>
							<!--Content goes here-->
							@yield('container')







						</p>
					</div>
				</div>
				<div class="col-sm-5">
					<img class="img-responsive" src="img/about1.png" alt="">
				</div>
			</div>
			<!--// end row -->
		</div>
		<!-- End What We Do -->

		<!-- Team v2 -->
		<div class="content-md container-sm">
			<div class="row">
				
				<div class="col-sm-7 col-sm-pull-5">
					<!-- End Team v2 -->
					<div class="row">
						<div class="col-xs-6 xs-full-width xs-margin-b-30">
							<div class="overflow-h">
								<!-- Team v2 -->
								<div class="team-v2 wow fadeInLeft" data-wow-duration=".2" data-wow-delay=".2s">
									<div class="team-v2-img-gradient">
										<img class="img-responsive" src="img/members/06.jpg" alt="">
									</div>
									<div class="team-v2-content">
										<div class="team-v2-center-align">
											<h4 class="team-v2-member">Sara Glaser</h4>
											<span class="team-v2-member-position">Director</span>
										</div>
									</div>
									<a class="team-v2-link" href="#"></a>
								</div>
							</div>
						</div>
						<div class="col-xs-6 xs-full-width">
							<div class="overflow-h">
								<!-- Team v2 -->
								<div class="team-v2 wow fadeInLeft" data-wow-duration=".2" data-wow-delay=".1s">
									<div class="team-v2-img-gradient">
										<img class="img-responsive" src="img/members/07.jpg" alt="">
									</div>
									<div class="team-v2-content">
										<div class="team-v2-center-align">
											<h4 class="team-v2-member">David Martin</h4>
											<span class="team-v2-member-position">Marketing</span>
										</div>
									</div>
									<a class="team-v2-link" href="#"></a>
								</div>
								<!-- End Team v2 -->
							</div>
						</div>
					</div>
					<!--// end row -->
					<!-- End Team v2 -->
				</div>
			</div>
			<!--// end row -->
		</div>
		<!-- End Team v2 -->


	</div>
	<!-- end service section -->


	


	

	
</main>
<!-- end main content -->

<!-- start footer area -->
<footer class="footer-area-content">

	<div class="container">
		<div class="footer-wrapper style-3">
			<div class="type-1">
				<div class="footer-columns-entry">
					<div class="row">
						<div class="col-md-3">
							<img alt="" src="img/footer-logo.png" class="footer-logo">
							<div class="footer-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</div>
							
							<div class="clear"></div>
						</div>
						
						
						<div class="clearfix visible-sm-block"></div>
						
					</div>
				</div>

			</div>
		</div>
	</div>


	<div class="footer-bottom footer-wrapper style-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="footer-bottom-navigation">
						<div class="cell-view">
							<div class="footer-links">
								<a href="#">Site Map</a>
								<a href="#">Search</a>
								<a href="#">Terms</a>
								<a href="#">Advanced Search</a>
								<a href="#">Orders and Returns</a>
								<a href="#">Contact Us</a>
							</div>
							<div class="copyright">Created with by <a target="_blank" href="http://iglyphic.com/">iGlyphic</a> . All right reserved</div>
						</div>
						<div class="cell-view">
							<div class="payment-methods">
								<a href="#"><img alt="" src="img/payment-method-1.png"></a>
								<a href="#"><img alt="" src="img/payment-method-2.png"></a>
								<a href="#"><img alt="" src="img/payment-method-3.png"></a>
								<a href="#"><img alt="" src="img/payment-method-4.png"></a>
								<a href="#"><img alt="" src="img/payment-method-5.png"></a>
								<a href="#"><img alt="" src="img/payment-method-6.png"></a>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>



</footer>
<!-- footer area end -->





</body>

</html>