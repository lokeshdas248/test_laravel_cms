<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Categories List</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

        <!-- Styles -->
        <style>
        
        </style>

        
    </head>
    <body>
        <p style="text-align:right;padding:20px;"><a href="add">Add New Category</a>
        <a href="../page/list">Manage Pages</a> </p>
        <p style="text-align:center;" class="text-success">{{session('msg')}}</p>
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">CATEGORY</th>
              <th scope="col">SLUG</th>              
              <th scope="col">ACTION</th>
            </tr>
          </thead>
          <tbody> 
            @foreach($catArr as $category)       
            <tr>
                <td>{{$category->id}}</td>
                <td>{{$category->category_name}}</td>
                <td>{{$category->category_slug}}</td>                
                <td><a href="edit/{{$category->id}}">Edit</a> / 
                  <a href="delete/{{$category->id}}">Delete</a></td>
            </tr>
            @endforeach

        </table>
    </body>
</html>
