<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Add New Page</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

        <!-- Styles -->
        <style>
        
        </style>

        
    </head>
    <body>
        <p style="text-align:right;padding:20px;"><a href="list">Page List</a></p>
        <form method="post" action="{{route('page.update', [$pageArr->id])}}">
          @csrf
        <table class="table" align="center" style="width:80%">
          <thead>
            <tr>
              <th scope="col" colspan="2">Create New Page</th>              
            </tr>
          </thead>
          <tbody>                
            <tr>
                <td>Page Name</td>
                <td><input type="text" name="page_name" value="{{$pageArr->page_name}}" required="required" /></td>                
            </tr>
            <tr>
                <td>Page Title</td>
                <td><input type="text" name="page_title" value="{{$pageArr->page_title}}" required="required" /></td>                
            </tr>
            <tr>
                <td>Slug</td>
                <td><input type="text" name="page_slug" value="{{$pageArr->page_slug}}" required="required" /></td>                
            </tr>
            <tr>
                <td>Category</td>
                <td>
                    <select name="parent_id" class="form-select">
                        <option value="0">--- Select ---</option>
                        @foreach($catArr as $category)
                        <option value="{{$category->id}}"  {{ ( $category->id == $pageArr->parent_id) ? 'selected' : '' }}>{{$category->category_slug}}</option>
                        @endforeach
                    </select>
                </td>                
            </tr>
            <tr>
                <td>Content</td>
                <td><textarea name="content" required="required" cols="100" rows="10">{{$pageArr->content}}</textarea>
                </td>                
            </tr>            
            <tr>
                <td></td>
                <td><button type="submit" name="btn_submit" class="btn btn-success">Update</button>
                  <a name="btn_cancel" class="btn btn-warning" href="../list">Cancel</a></td>                
            </tr>
        </table>
      </form>
    </body>
</html>
