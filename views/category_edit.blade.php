<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Update Category</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

        <!-- Styles -->
        <style>
        
        </style>

        
    </head>
    <body>
        <p style="text-align:right;padding:20px;"><a href="../list">Category List</a></p>
        <form method="post" action="{{route('category.update', [$categoryArr->id])}}">
          @csrf
        <table class="table" align="center" style="width:50%">
          <thead>
            <tr>
              <th scope="col" colspan="2">Edit Record</th>              
            </tr>
          </thead>
          <tbody> 
                
            <tr>
                <td>Category Name</td>
                <td><input type="text" name="category_name" value="{{$categoryArr->category_name}}" required="required" /></td>                
            </tr>
            <tr>
                <td>Category Slug</td>
                <td><input type="text" name="category_slug" value="{{$categoryArr->category_slug}}" required="required" /></td>                
            </tr>
            <tr>
                <td>Parent Category</td>
                <td>
                    <select name="parent_category" class="form-select">                        
                        <option value="0">--- Main ---</option>
                        @foreach($Categories as $category)
                        <option value="{{$category->id}}" {{ ( $category->id == $categoryArr->parent_id) ? 'selected' : '' }}> {{$category->category_name}}</option>
                        @endforeach
                    </select>
                </td>                
            </tr>            
            <tr>
                <td></td>
                <td><button type="submit" name="btn_submit" class="btn btn-success">Update</button>
                <a name="btn_cancel" class="btn btn-warning" href="../list">Cancel</a></td>                
            </tr>
        </table>
      </form>
    </body>
</html>
